package net.eldiosantos.scala.feed.parser.service

import net.eldiosantos.scala.feed.model.domain.FeedSpec
import org.apache.logging.log4j.scala.Logging
import org.specs2.mutable.Specification
import org.specs2.specification.Scope

class RssFetcherServiceTest extends Specification with Logging {
  "service with Nerdcast feed" should {
    "fetch should return Nerdcast feed items" in new readNerdcastFile {
      logger.info("starting...")

      feedResult.title mustEqual "NerdCast"

      logger.info(s"feed size: ${feedResult.feed.size}")

      logger.info(s"feed:\n\r- ${feedResult.title}")
      logger.info(s"feed items:\n\r- ${feedResult.feed.map {_.title}.mkString("\n\t- ")}")

      feedResult.feed.size mustEqual 650

      ok
    }

    "all items should return a diferent id [Nerdcast]" in new readNerdcastFile {
      logger.info("starting...")

      val feed = feedResult.feed

      feed.map {_.id}.distinct.size mustEqual feed.size
    }

    "all items should return a diferent id [G1]" in new readG1File {
      logger.info("starting...")

      val feed = feedResult.feed

      feed.map {_.id}.distinct.size mustEqual feed.size
    }

    "get feed without error" in {
      new RssFetcherService().fetch(FeedSpec("http://g1.globo.com/dynamo/rss2.xml"))
      ok
    }

/*
    "get feed without error [2]" in {
      new RssFetcherService().fetch(FeedSpec("https://jovemnerd.com.br/feed-nerdcast/"))
      ok
    }
*/

    "fetch a feed without podcasts" in new readG1File {
      logger.info(s"G1 feed size: ${feedResult.feed.size}")
      feedResult.feed.size mustEqual 40

      logger.info(s"G1 first feed item titlle: ${feedResult.feed(0).title}")
      feedResult.feed(0).title mustEqual "Motorista abandona carro após capotamento em Tatuí"

      logger.info(s"G1 first feed item url: ${feedResult.feed(0).url}")
      feedResult.feed(0).url mustEqual "https://g1.globo.com/sp/itapetininga-regiao/noticia/motorista-abandona-carro-apos-capotamento-em-tatui.ghtml"

      logger.info(s"G1 first feed item episode url: ${feedResult.feed(0).episodeUrl}")
      feedResult.feed(0).episodeUrl.isEmpty must beTrue
    }
  }

  "Nerdcast must be a podcast" in new readNerdcastFile {
    feedResult.feed.exists { _.isPodcast } must beTrue
    feedResult.feed.exists {!_.episodeUrl.isEmpty} must beTrue
    feedResult.isPodcast must beTrue
  }

  "G1 must not be a podcast" in new readG1File {
    feedResult.feed.exists { _.isPodcast } must beFalse
    feedResult.feed.exists {!_.episodeUrl.isEmpty} must beFalse
    feedResult.isPodcast must beFalse
  }

  "G1 must not have a null episodeUrl" in new readG1File {
    feedResult.feed.exists { _.episodeUrl == null } must beFalse
    feedResult.feed.exists { _.episodeUrl.isEmpty} must beTrue
  }

  "Fetch Cinema com Rapadura" in {
    new RssFetcherService().fetch(FeedSpec("https://cinemacomrapadura.com.br/feed/"))
    ok
  }

  "Fetch Rapaduracast" in {
    val feed = new RssFetcherService().fetch(FeedSpec("http://feeds.feedburner.com/rapaduracast"))
    println(s"url: ${feed.url}")
    println(s"feed: ${feed.title}")
    println(s"items: ${feed.feed.size}")
    println(s"last item title: ${feed.feed.head.title}")
    println(s"last item episodeUrl: ${feed.feed.head.episodeUrl}")
    println(s"last item url: ${feed.feed.head.url}")
    println(s"last item id: ${feed.feed.head.id}")
    ok
  }

  trait readNerdcastFile extends Scope {
    val service = new RssFetcherService((url: String) => scala.io.Source.fromFile("./parser/src/test/resources/request_results/nerdcast_feed.xml").mkString)
    val feedResult = service.fetch(FeedSpec("https://jovemnerd.com.br/feed-nerdcast/", "nerdcast"))
  }

  trait readG1File extends Scope {
    val service = new RssFetcherService((url: String) => scala.io.Source.fromFile("./parser/src/test/resources/request_results/feed_without_episodes.xml").mkString)
    val feedResult = service.fetch(FeedSpec("http://g1.globo.com/dynamo/rss2.xml", "error-feed"))
  }

}
