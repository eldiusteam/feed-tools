package net.eldiosantos.scala.feed.parser.service

trait HttpGetService {
  def fetchUrl(url: String): String
}
