package net.eldiosantos.scala.feed.parser.service

import java.net.{HttpURLConnection, URL}

class DefaultHttpGetService extends HttpGetService {
  //override def fetchUrl(url: String): String = scala.io.Source.fromURL(url, "utf-8").mkString
  override def fetchUrl(url: String): String = {
    val conn = new URL(url).openConnection().asInstanceOf[HttpURLConnection]
    conn.setRequestProperty("User-Agent", "Feed Fetcher Agent")
    conn.setRequestProperty("Accept", "*/*")

    scala.io.Source.fromInputStream(conn.getInputStream, "utf-8").mkString
  }
}
