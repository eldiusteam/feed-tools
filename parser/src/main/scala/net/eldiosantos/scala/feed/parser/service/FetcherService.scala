package net.eldiosantos.scala.feed.parser.service

import net.eldiosantos.scala.feed.model.domain.{Feed, FeedItem, FeedSpec}

import scala.xml.Elem

trait FetcherService {

  val client: HttpGetService

  def fetch(feed: FeedSpec): Feed

  def fetchUrl(feed: FeedSpec) = {
    client.fetchUrl(feed.url)
  }

  def getTitle(xml: Elem): String

  def getDescription(xml: Elem): String

  def getItems(xml: Elem): List[FeedItem]

  def getLanguage(xml: Elem): String

  def getCopyright(xml: Elem): String

  def getLastBuildDate(xml: Elem): String

  def getImageUrl(xml: Elem): String

  def getImageTitle(xml: Elem): String

}
