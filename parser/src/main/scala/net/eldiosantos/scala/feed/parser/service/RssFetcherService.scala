package net.eldiosantos.scala.feed.parser.service

import net.eldiosantos.scala.feed.model.domain.{Feed, FeedItem, FeedSpec}
import org.apache.logging.log4j.scala.Logging

import scala.xml.{Elem, XML}

class RssFetcherService(val client: HttpGetService = new DefaultHttpGetService) extends FetcherService with Logging {

  override def fetch(feed: FeedSpec): Feed = {

    logger.info(s"Starting to fetch '${feed.url}'")

    val xml = toXML(feed)

  /**
    * url: String
    * , title: String
    * , description: String
    * , language: String
    * , copyright: String
    * , lastBuildDate: String
    * , imageUrl: String
    * , imageTitle: String
    * , feed: List[FeedItem]
    */
    Feed(feed.url, getTitle(xml), getDescription(xml), getLanguage(xml), getCopyright(xml), getLastBuildDate(xml), getImageUrl(xml), getImageTitle(xml), getItems(xml))
  }

  private def toXML(feed: FeedSpec) = {
    XML.loadString(fetchUrl(feed))
  }

  override def getTitle(xml: Elem): String = {
    (xml \\ "channel" \ "title").text
  }

  override def getDescription(xml: Elem): String = {
    (xml \\ "channel" \ "description").text
  }

  override def getLanguage(xml: Elem): String = {
    (xml \\ "channel" \ "language").text
  }

  override def getCopyright(xml: Elem): String = {
    (xml \\ "channel" \ "copyright").text
  }

  override def getLastBuildDate(xml: Elem): String = {
    (xml \\ "channel" \ "lastBuildDate").text
  }

  override def getImageUrl(xml: Elem): String = {
    (xml \\ "channel" \ "image" \ "url").text
  }

  override def getImageTitle(xml: Elem): String = {
    (xml \\ "channel" \ "image" \ "title").text
  }

  /*
    id: String
    , url: String
    , title: String
    , content: String
    , episodeUrl: String
    , episodeLength: String
    , episodeType: String
    , pubDate: String
   */
  override def getItems(xml: Elem): List[FeedItem] = {
    (xml \\ "channel" \ "item")
      .map (e => FeedItem(
        (e \ "guid").text
        , (e \ "link").text
        , (e \ "title").text
        , (e \ "description").text
        , (e \ "enclosure" \ "@url").text
        , (e \ "enclosure" \ "@length").text
        , (e \ "enclosure" \ "@type").text
        , (e \ "pubDate").text)
      )
      .toList
  }

}

object RssFetcherService {
  def factory() = new RssFetcherService()
  def factory(client: HttpGetService) = new RssFetcherService(client)
}
