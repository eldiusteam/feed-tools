package net.eldiosantos.scala.feed.fetcher

import net.eldiosantos.scala.feed.fetcher.service.AsyncFetcherService
import org.apache.logging.log4j.scala.Logging

import scala.io.StdIn

object AsyncFetcherAppObject extends App with Logging {
  logger.info("Starting agent")

  AsyncFetcherService.start()

  while (true) {
    StdIn.readLine()
    Thread.sleep(10000L)
  }
}
