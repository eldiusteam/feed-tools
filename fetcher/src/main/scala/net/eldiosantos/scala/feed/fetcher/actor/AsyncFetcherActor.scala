package net.eldiosantos.scala.feed.fetcher.actor

import akka.actor.Actor
import net.eldiosantos.scala.feed.fetcher.actor.FetcherActor.{FetchAll, FetchSingle}
import net.eldiosantos.scala.feed.model.domain.FeedSpec
import net.eldiosantos.scala.feed.parser.service.RssFetcherService
import net.eldiosantos.scala.feed.persistence.AsyncFeedRepository
import org.apache.logging.log4j.scala.Logging

class AsyncFetcherActor extends Actor with Logging {
  private val service = RssFetcherService.factory()

  override def receive: Receive = {
    case FetchSingle(f) => {
      logger.info(s"Received a fetch message for ${f.url}")
      logger.debug(s"actor: ${self.path.address}/${self.path}")
      AsyncFeedRepository.save(service.fetch(f))
    }
    case FetchAll(func) => func().foreach(context.self ! FetchSingle(_))
    case feed: FeedSpec => self ! FetchSingle(feed)
  }
}


