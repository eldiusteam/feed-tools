package net.eldiosantos.scala.feed.fetcher

import net.eldiosantos.scala.feed.fetcher.service.FetcherService
import org.apache.logging.log4j.scala.Logging

import scala.io.StdIn

object AlternateFetcherAppObject extends App with Logging {
  logger.info("Starting agent")

  FetcherService.start()

  while (true) {
    StdIn.readLine()
    Thread.sleep(10000L)
  }
}
