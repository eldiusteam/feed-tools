package net.eldiosantos.scala.feed.fetcher.service

import akka.actor.{ActorSystem, Cancellable, Props}
import akka.routing.RoundRobinPool
import net.eldiosantos.scala.feed.config.service.{FetcherConfigService, PersistenceConfigService}
import net.eldiosantos.scala.feed.fetcher.actor.DatabaseWatcher.Watch
import net.eldiosantos.scala.feed.fetcher.actor.FetcherActor.{FetchAll, FetchSingle}
import net.eldiosantos.scala.feed.fetcher.actor.{AsyncFetcherActor, DatabaseWatcher}
import net.eldiosantos.scala.feed.model.domain.FeedSpec
import net.eldiosantos.scala.feed.persistence.AsyncFeedRepository
import org.apache.logging.log4j.scala.Logging

import scala.concurrent.Await
import scala.concurrent.duration._

class AsyncFetcherService extends Logging {
  private val config = FetcherConfigService().load()

  private val system = ActorSystem("async-feed-fetcher-system")

  private val fetcher = system.actorOf(Props[AsyncFetcherActor].withRouter(new RoundRobinPool(config.workers)), "async-fetcher")
  private val watcher = system.actorOf(Props[DatabaseWatcher], "db-watcher")

  implicit val ec = system.dispatcher

  def start(): Cancellable = {
    logger.info("Starting async actor scheduler...")

    if (PersistenceConfigService().load().debugConnectionPool) {
      system.scheduler.schedule(
        0 seconds
        , 30 seconds
        , watcher
        , Watch
      )
    }
    system.scheduler.schedule(
      config.startDelayInMinutes minutes
      , config.updateIntervalInMinutes minutes
      , fetcher
      , FetchAll(() => {
        Await.result(AsyncFeedRepository.stack, 30 seconds)
      })
    )
  }
  def stop(): Unit = system.stop(fetcher)
  def add(f: FeedSpec): Unit = fetcher ! FetchSingle(f)
}

object AsyncFetcherService {
  private val service = new FetcherService

  def start(): Cancellable = service.start()
  def stop(): Unit = service.stop()
  def add(f: FeedSpec): Unit = service.add(f)
}

