package net.eldiosantos.scala.feed.fetcher.actor

import akka.actor.Actor
import net.eldiosantos.scala.feed.fetcher.actor.DatabaseWatcher.Watch
import net.eldiosantos.scala.feed.persistence.util.Database
import org.apache.logging.log4j.scala.Logging

class DatabaseWatcher extends Actor with Logging {
  override def receive: Receive = {
    case Watch =>
      logger.info(s"""
        |------------------------
        |- WATCHER --------------
        |${Database().status()}
        |------------------------
      """.stripMargin)
  }
}

object DatabaseWatcher {
  object Watch
}
