package net.eldiosantos.scala.feed.fetcher.actor

import java.sql.{DriverManager, ResultSet}

import net.eldiosantos.scala.feed.persistence.util.DB
import org.apache.logging.log4j.scala.Logging

object TestApp extends App with Logging {
  def fetch(rs: ResultSet): List[String] = {
    if(rs.next()) rs.getString(1) :: fetch(rs)
    else Nil
  }

  DB.executeOnSession { conn =>
    fetch(conn.createStatement().executeQuery("show tables")).foreach(println)
  }

  val conn = DriverManager.getConnection("jdbc:mysql://localhost/development", "feedApp", "123Senha")

  logger.info(
    s"""
      |connection status: ${conn.isValid(100)}
      |tables:
      |- ${fetch(conn.createStatement().executeQuery("show tables")).mkString("\n- ")}
    """.stripMargin)
}
