package net.eldiosantos.scala.feed.fetcher

import net.eldiosantos.scala.feed.fetcher.service.FetcherService
import org.apache.logging.log4j.scala.Logging

import scala.io.StdIn

object FetcherAppObject extends App with Logging {
  logger.info("Starting agent")

  FetcherService.start()

  StdIn.readLine("Press ENTER to stop...") // let it run until user presses return

  FetcherService.stop()
  System.exit(0)
}
