package net.eldiosantos.scala.feed.fetcher.actor

import akka.actor.Actor
import net.eldiosantos.scala.feed.fetcher.actor.FetcherActor.{FetchAll, FetchSingle}
import net.eldiosantos.scala.feed.model.domain.FeedSpec
import net.eldiosantos.scala.feed.parser.service.RssFetcherService
import net.eldiosantos.scala.feed.persistence.FeedRepository
import net.eldiosantos.scala.feed.persistence.util.DB
import org.apache.logging.log4j.scala.Logging

class FetcherActor extends Actor with Logging {
  private val service = RssFetcherService.factory()

  override def receive: Receive = {
    case FetchSingle(f) => {
      logger.info(s"Received a fetch message for ${f.url}")
      logger.debug(s"actor: ${self.path.address}/${self.path}")
      DB.executeOnSession{conn =>
        try {
          new FeedRepository(conn).save(service.fetch(f))
        } catch {
          case t: Throwable => logger.error(s"Error fetching feed for ${f.url}", t)
        }
      }
    }
    case FetchAll(func) => func().foreach(context.self ! FetchSingle(_))
    case feed: FeedSpec => self ! FetchSingle(feed)
  }
}

object FetcherActor {
  case class FetchSingle(feed: FeedSpec)
  case class FetchAll(f: () => List[FeedSpec])
}
