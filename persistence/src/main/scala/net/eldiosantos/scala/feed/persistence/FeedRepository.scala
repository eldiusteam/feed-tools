package net.eldiosantos.scala.feed.persistence

import java.sql.{Connection, ResultSet, SQLException}

import net.eldiosantos.scala.feed.model.domain.{Feed, FeedItem, FeedSpec}
import net.eldiosantos.scala.feed.persistence.util.DB
import org.apache.logging.log4j.scala.Logging

class FeedRepository(conn: Connection) extends Logging {

  def save(feed: Feed): Feed = {
    try {
      val insertFeedSt = DB.prepareStatement("insert_feed", conn)
      insertFeedSt.setString(1, feed.url)
      insertFeedSt.setString(2, feed.title)
      insertFeedSt.setString(3, feed.description)
      insertFeedSt.setString(4, feed.language)
      insertFeedSt.setString(5, feed.copyright)
      insertFeedSt.setString(6, feed.lastBuildDate)
      insertFeedSt.setString(7, feed.imageUrl)
      insertFeedSt.setString(8, feed.imageTitle)

      insertFeedSt.execute()

      insertFeedSt.close()

      save(feed.feed, feed.title)

    } catch {
      case _: SQLException => {
        update(feed)
      }
    }
    feed
  }

  def update(feed: Feed): Feed = {
    val updateFeedSt = DB.prepareStatement("update_feed", conn)

    updateFeedSt.setString(8, feed.url)
    updateFeedSt.setString(1, feed.title)
    updateFeedSt.setString(2, feed.description)
    updateFeedSt.setString(3, feed.language)
    updateFeedSt.setString(4, feed.copyright)
    updateFeedSt.setString(5, feed.lastBuildDate)
    updateFeedSt.setString(6, feed.imageUrl)
    updateFeedSt.setString(7, feed.imageTitle)

    updateFeedSt.execute()

    updateFeedSt.close()

    save(feed.feed, feed.title)

    feed
  }

  def save(items: List[FeedItem], feedName: String): List[FeedItem] = {
    items.map(i => save(i, feedName))
  }

  def save(item: FeedItem, feedName: String): FeedItem = {
    try {
      val insertItemSt = DB.prepareStatement("insert_item", conn)
      /*
        1 id
        ,2 url
        ,3 title
        ,4 content
        ,5 episode_url
        ,6 episode_length
        ,7 episode_type
        ,8 pub_date
        ,9 feed_title
      */
      insertItemSt.setString(1, item.id)
      insertItemSt.setString(2, item.url)
      insertItemSt.setString(3, item.title)
      insertItemSt.setString(4, item.content)
      insertItemSt.setString(5, item.episodeUrl)
      insertItemSt.setString(6, item.episodeLength)
      insertItemSt.setString(7, item.episodeType)
      insertItemSt.setString(8, item.pubDate)
      insertItemSt.setString(9, feedName)

      insertItemSt.execute()

      insertItemSt.close()

    } catch {
      case _: SQLException => {
        update(item, feedName)
      }
      case ex: Exception => {
        logger.error(s"Skipping...\n[${feedName}]${item.toString}", ex)
      }
    }
    item
  }

  def update(item: FeedItem, feedName: String): FeedItem = {
    try {
      val updateItemSt = DB.prepareStatement("update_item", conn)
      updateItemSt.setString(8, item.url)
      updateItemSt.setString(1, item.title)
      updateItemSt.setString(2, item.content)
      updateItemSt.setString(3, item.episodeUrl)
      updateItemSt.setString(4, item.episodeLength)
      updateItemSt.setString(5, item.episodeType)
      updateItemSt.setString(6, item.pubDate)
      updateItemSt.setString(7, feedName)

      updateItemSt.execute()

      updateItemSt.close()
    } catch {
      case ex: SQLException => {
        logger.error(s"Skipping...\n[${feedName}]${item.toString}", ex)
      }
      case ex: Exception => {
        logger.error(s"Skipping...\n[${feedName}]${item.toString}", ex)
      }
    }
    item
  }

  def list: List[Feed] = {
    val listFeedSt = DB.prepareStatement("list_feed", conn)
    fetch(listFeedSt.executeQuery())
  }

  def listItems(feedName: String): List[FeedItem] = {
    val listItemSt = DB.prepareStatement("list_item", conn)
    listItemSt.setString(1, feedName)

    fetchItems(listItemSt.executeQuery())
  }

  /**
    * url
    * , title
    * , description
    * , lang
    * , copyright
    * , last_build_date
    * , image_url
    * , image_title
    *
    * @param rs
    * @return
    */
  def fetchSingle(rs: ResultSet): Feed = Feed(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), listItems(rs.getString(2)))
  //def fetchSingle(rs: ResultSet): Feed = Feed(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), Nil)

  def fetch(rs: ResultSet): List[Feed] = {
    if (rs.next()) fetchSingle(rs) :: fetch(rs)
    else Nil
  }

  /**
    * id
    * , url
    * , title
    * , content
    * , episode_url
    * , episode_length
    * , episode_type
    * , pub_date
    * , feed_title
    * @param rs
    * @return
    */
  def fetchSingleItem(rs: ResultSet): FeedItem = FeedItem(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(6), rs.getString(6))

  def fetchItems(rs: ResultSet): List[FeedItem] = {
    if (rs.next()) fetchSingleItem(rs) :: fetchItems(rs)
    else Nil
  }

  def fetchSingleSimplified(rs: ResultSet) = Feed(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), Nil)
  def fetchSimplified(rs: ResultSet): List[Feed] = {
    if (rs.next()) fetchSingleSimplified(rs) :: fetchSimplified(rs)
    else Nil
  }

  def listSimplified: List[Feed] = {
    val listFeedSt = DB.prepareStatement("list_feed", conn)
    fetchSimplified(listFeedSt.executeQuery())
  }

  def fetchStack(rs: ResultSet): List[FeedSpec] = {
    if (rs.next()) FeedSpec(rs.getString(1)) :: fetchStack(rs)
    else Nil
  }

  def stack: List[FeedSpec] = {
    val listStackItems = DB.prepareStatement("list_stack", conn)
    fetchStack(listStackItems.executeQuery())
  }
}
