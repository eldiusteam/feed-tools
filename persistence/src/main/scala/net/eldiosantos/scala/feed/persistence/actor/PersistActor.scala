package net.eldiosantos.scala.feed.persistence.actor

import akka.actor.Actor
import net.eldiosantos.scala.feed.model.domain.Feed
import net.eldiosantos.scala.feed.persistence.FeedRepository
import net.eldiosantos.scala.feed.persistence.actor.PersistActor.{FetchStack, ListItens, ListSimplified, PersistFeed}
import net.eldiosantos.scala.feed.persistence.util.DB

class PersistActor extends Actor {
  override def receive: Receive = {
    case PersistFeed(f) => DB.executeOnSession(new FeedRepository(_).save(f))
    case FetchStack => sender() ! DB.execute(new FeedRepository(_).stack)
    case ListSimplified => sender() ! DB.execute(new FeedRepository(_).listSimplified)
    case ListItens(name) => sender() ! DB.execute(new FeedRepository(_).listItems(name))
  }
}

object PersistActor {
  case class PersistFeed(feed: Feed)
  case class ListItens(name: String)
  object ListSimplified
  object FetchStack
}
