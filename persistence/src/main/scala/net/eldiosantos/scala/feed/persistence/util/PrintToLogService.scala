package net.eldiosantos.scala.feed.persistence.util

import java.io.{ByteArrayOutputStream, PrintWriter}

import org.apache.logging.log4j.scala.Logging

class PrintToLogService {
  private class CustomOutputStream extends ByteArrayOutputStream with Logging {
    override def flush(): Unit = {
      super.flush()
      logger.info(toString("utf-8"))
      super.reset()
    }

    override def write(b: Array[Byte]): Unit = {
      super.write(b)
      logger.info(super.toString("utf-8"))
      super.reset()
    }
  }

  private val printer = new PrintWriter(new CustomOutputStream)

  def printWriter: PrintWriter = printer
}

object PrintToLogService {
  private val printer = new PrintToLogService
  def apply(): PrintWriter = printer.printWriter
}

