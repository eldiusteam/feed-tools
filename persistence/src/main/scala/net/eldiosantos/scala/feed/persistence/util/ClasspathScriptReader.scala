package net.eldiosantos.scala.feed.persistence.util

object ClasspathScriptReader {

  def readScript(name: String, vendor: String = "h2"): String = {
    try {
      scala.io.Source.fromResource(s"sql/$vendor/$name.sql").mkString
    } catch {
      case e: Throwable => scala.io.Source.fromResource(s"sql/$name.sql").mkString
    }
  }

  def readBatchScript(name: String, vendor: String = "h2"): List[String] = {
    readScript(name, vendor)
        .split(";")
          .filterNot(_.trim.isEmpty)
            .toList
  }

}
