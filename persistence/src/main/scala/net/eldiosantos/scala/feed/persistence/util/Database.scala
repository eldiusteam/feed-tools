package net.eldiosantos.scala.feed.persistence.util

import java.sql.Connection
import java.util.concurrent.atomic.AtomicLong

import javax.sql.DataSource
import net.eldiosantos.scala.feed.config.model.PersistenceConfig
import net.eldiosantos.scala.feed.config.service.PersistenceConfigService
import org.apache.commons.dbcp2._
import org.apache.logging.log4j.scala.Logging

import scala.collection.mutable.Buffer

class Database(config: PersistenceConfig) extends Logging {

  private val counter = new AtomicLong(0)
  private val commitCounter = new AtomicLong(0)
  private val releaseCounter = new AtomicLong(0)
  private val connectionsSeq = Buffer[Connection]()

  logger.info("Starting database connection...")

  private def setupDatasource(): DataSource = {
    val connectionPool = new BasicDataSource()

    if (config.driver.nonEmpty) connectionPool.setDriverClassName(config.driver.get)

    connectionPool.setUrl(config.url)
    connectionPool.setUsername(config.user)
    connectionPool.setPassword(config.pass)
    connectionPool.setMaxIdle(config.maxIdleConnections)
    connectionPool.setMaxTotal(config.maxTotalConnections)

    connectionPool.setTimeBetweenEvictionRunsMillis(1000)

    connectionPool.setDefaultQueryTimeout(100)
    connectionPool.setFastFailValidation(true)
    connectionPool.setInitialSize(config.maxIdleConnections)

    connectionPool
  }

  private val datasource = setupDatasource()

  private def readBatchScript(name: String, conn: Connection): List[String] = {
    ClasspathScriptReader.readBatchScript(name, conn.getMetaData.getDatabaseProductName.toLowerCase)
  }

  private def prepare(conn: Connection): Unit = {
    try {
      val st = conn.createStatement()
      readBatchScript("create_tables", conn)
        .foreach(sql => {
          st.addBatch(sql)
          logger.debug(
            s"""
               |adding query:
               |$sql
              """.stripMargin)
        })

      logger.info(
        s"""
           |#############################################
           |Executed create table:
           |- ${st.executeBatch().mkString("\n- ")}
           |#############################################
           |""".stripMargin)

      st.close()
    } catch {
      case e: Throwable => logger.warn("Error creating tables (maybe they already created...)", e)
    }
    try {
      val st = conn.createStatement()
      readBatchScript("populate_database", conn)
        .foreach(sql => {
          st.addBatch(sql)
          logger.debug(
            s"""
               |---
               |adding query:
               |$sql
               |---
              """.stripMargin)
        })

      logger.info(
        s"""
           |#############################################
           |Executed populate tables:
           |- ${st.executeBatch().mkString("\n- ")}
           |#############################################
           |""".stripMargin)

      st.close()
    } catch {
      case e: Throwable => logger.warn("Error populating database (maybe they already populated...)", e)
    } finally {
      printStatus()
    }
    release(conn)
  }

  def connection(): Connection = {
    val conn = datasource.getConnection
    connectionsSeq += conn
    counter.incrementAndGet()
    datasource.asInstanceOf[BasicDataSource]
    val connectionPool = datasource.asInstanceOf[BasicDataSource]
    val stack = Thread.getAllStackTraces().get(Thread.currentThread()).toList
    if (config.debugConnectionPool) {
      logger.debug(
        s"""
         |- get connection -------
         | connection is valid? ${conn.isValid(config.validationTimeout)}
         | total active: ${connectionPool.getNumActive}
         | total idle:  ${connectionPool.getNumIdle}
         | max idle: ${connectionPool.getMaxIdle}
         | max total connections: ${connectionPool.getMaxTotal}
         | stack: ${stack(3)}
         | stack2: ${stack(4)}
         | class: ${conn.getClass}
         | counter: ${counter.get()}
         | commitCounter: ${commitCounter.get()}
         |-----------------------
       """.stripMargin)
    }

    if (conn.isValid(10)) conn
    else {
      release(conn)
      connection()
    }
  }

  def printStatus(): Unit = {
    logger.debug(status())
  }

  def status(): String = {
    val ds = datasource.asInstanceOf[BasicDataSource]
    val stack = Thread.getAllStackTraces().get(Thread.currentThread()).toList

    s"""
       |- status ---------------
       | total active: ${ds.getNumActive}
       | total idle:  ${ds.getNumIdle}
       | max idle: ${ds.getMaxIdle}
       | max total connections: ${ds.getMaxTotal}
       | stack: ${stack(3)}
       | stack2: ${stack(4)}
       | counter: ${counter.get()}
       | commitCounter: ${commitCounter.get()}
       | releaseCounter: ${releaseCounter.get()}
       | collection: ${connectionsSeq.size}
       |------------------------
       """.stripMargin
  }

  def commit(conn: Connection): Unit = {
    logger.debug("commiting transaction...")
    commitCounter.incrementAndGet()
    if(!conn.getAutoCommit) conn.commit()
    release(conn)
  }

  def rollback(conn: Connection): Unit = {
    logger.debug("commiting transaction...")
    if(!conn.getAutoCommit) conn.rollback()
    release(conn)
  }

  def release(conn: Connection): Unit = {
    val ds = datasource.asInstanceOf[BasicDataSource]
    ds.invalidateConnection(conn)
    releaseCounter.incrementAndGet()
    val index = connectionsSeq.indexOf(conn)
    if (index < 0) {
      throw new IllegalStateException("What the hell is this connection doing here?")
    }
    connectionsSeq.remove(index)
    printStatus()
  }

  prepare(this.connection())

}

object Database {
  val database = new Database(PersistenceConfigService().load())

  def apply(): Database = database

  def apply(config: PersistenceConfig): Database = new Database(config)
}
