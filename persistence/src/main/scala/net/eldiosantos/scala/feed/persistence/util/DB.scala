package net.eldiosantos.scala.feed.persistence.util

import java.sql.{Connection, PreparedStatement}

import org.apache.logging.log4j.scala.Logging

object DB extends Logging {
  private val database = Database()

  def readScript(name: String, conn: Connection): String = ClasspathScriptReader.readScript(name, conn.getMetaData.getDatabaseProductName.toLowerCase)

  def prepareStatement(name: String, conn: Connection): PreparedStatement = conn.prepareStatement(readScript(name, conn))

  def commit(conn: Connection): Unit = {
    database.commit(conn)
  }

  def executeOnSession[A](f: Connection => A): A = {
    val conn = database.connection()
    try {
      val result = f(conn)
      database.commit(conn)
      result
    } catch {
      case t: Throwable => {
        logger.warn("Error executing transaction on database.", t)
        database.rollback(conn)
        throw t
      }
    }

  }

  def execute[A](f: Connection => A): A = {
    val conn = database.connection()
    val result = f(conn)
    database.release(conn)
    result
  }

}
