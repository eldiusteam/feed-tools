package net.eldiosantos.scala.feed.persistence

import akka.actor.{ActorSystem, Props}
import akka.routing.RoundRobinPool
import net.eldiosantos.scala.feed.config.service.PersistenceConfigService
import net.eldiosantos.scala.feed.model.domain.{Feed, FeedItem, FeedSpec}
import net.eldiosantos.scala.feed.persistence.actor.PersistActor
import net.eldiosantos.scala.feed.persistence.actor.PersistActor.{FetchStack, ListSimplified, PersistFeed}

import scala.concurrent.duration._
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.Future

class AsyncFeedRepository {
  private implicit val timeout = Timeout(5 seconds)

  private val config = PersistenceConfigService.apply().load()
  private val system = ActorSystem("feed-persistence-system")
  private val actor = system.actorOf(Props[PersistActor].withRouter(new RoundRobinPool(config.workers)), "persister")
  import system.dispatcher

  def save(feed: Feed): Unit = actor ! PersistFeed(feed)
  def list(): Future[List[Feed]] = (actor ? ListSimplified).mapTo[List[Feed]]
  def get(name: String): Future[List[FeedItem]] = (actor ? ListSimplified).mapTo[List[FeedItem]]
  def stack: Future[List[FeedSpec]] = (actor ? FetchStack).mapTo[List[FeedSpec]]
}

object AsyncFeedRepository {
  val repository = new AsyncFeedRepository

  def save(feed: Feed): Unit = repository.save(feed)

  def list(): Future[List[Feed]] = repository.list()

  def get(name: String): Future[List[FeedItem]] = repository.get(name)

  def stack: Future[List[FeedSpec]] = repository.stack
}
