
-- INSERT INITIAL FEEDS
INSERT INTO FETCHER_STACK (url) VALUES ('https://jovemnerd.com.br/feed-nerdcast/');

INSERT INTO FETCHER_STACK (url) VALUES ('http://g1.globo.com/dynamo/rss2.xml');

INSERT INTO FETCHER_STACK (url) VALUES ('https://rss.tecmundo.com.br/feed');
INSERT INTO FETCHER_STACK (url) VALUES ('http://gizmodo.uol.com.br/feed/');
INSERT INTO FETCHER_STACK (url) VALUES ('http://feeds.bbci.co.uk/portuguese/rss.xml');
INSERT INTO FETCHER_STACK (url) VALUES ('http://www.bbc.com/portuguese/topicos/brasil/index.xml');
INSERT INTO FETCHER_STACK (url) VALUES ('https://noticias.r7.com/feed.xml');
INSERT INTO FETCHER_STACK (url) VALUES ('https://noticias.r7.com/rio-de-janeiro/feed.xml');
INSERT INTO FETCHER_STACK (url) VALUES ('http://rss.home.uol.com.br/index.xml');
INSERT INTO FETCHER_STACK (url) VALUES ('http://tecnologia.uol.com.br/ultnot/index.xml');
INSERT INTO FETCHER_STACK (url) VALUES ('http://rss.uol.com.br/feed/economia.xml');
INSERT INTO FETCHER_STACK (url) VALUES ('http://rss.uol.com.br/feed/noticias.xml');
