

INSERT INTO FEED_ITEM (
  id
  , url
  , title
  , content
  , episode_url
  , episode_length
  , episode_type
  , pub_date
  , feed_title
  , last_updated
) VALUES (
  ?
  , ?
  , ?
  , ?
  , ?
  , ?
  , ?
  , ?
  , ?
  , CURRENT_TIMESTAMP
)
