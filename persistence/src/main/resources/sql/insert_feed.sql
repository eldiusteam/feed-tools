
INSERT INTO FEED_INFO (
  url
  , title
  , description
  , lang
  , copyright
  , last_build_date
  , image_url
  , image_title
  , last_updated
) VALUES (
  ?
  , ?
  , ?
  , ?
  , ?
  , ?
  , ?
  , ?
  , CURRENT_TIMESTAMP
)
