
UPDATE FEED_INFO
SET
  title = ?
  , description = ?
  , lang = ?
  , copyright = ?
  , last_build_date = ?
  , image_url = ?
  , image_title = ?
  , last_updated = CURRENT_TIMESTAMP
WHERE
  url = ?
