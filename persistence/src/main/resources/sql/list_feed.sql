
SELECT
  url
  , title
  , description
  , lang
  , copyright
  , last_build_date
  , image_url
  , image_title
FROM
  FEED_INFO
