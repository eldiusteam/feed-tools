
SELECT
  id
  , url
  , title
  , content
  , episode_url
  , episode_length
  , episode_type
  , pub_date
  , feed_title
FROM
  FEED_ITEM
WHERE
  feed_title = ?
