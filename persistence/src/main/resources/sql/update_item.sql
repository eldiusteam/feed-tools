

UPDATE FEED_ITEM
SET
  title = ?
  , content = ?
  , episode_url = ?
  , episode_length = ?
  , episode_type = ?
  , pub_date = ?
  , feed_title = ?
  , last_updated = CURRENT_TIMESTAMP
WHERE
  url = ?
