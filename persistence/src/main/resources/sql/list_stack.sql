
SELECT
  s.url
FROM
  FETCHER_STACK s
WHERE
  s.url NOT IN (
    SELECT i.url FROM FEED_INFO i
  )
