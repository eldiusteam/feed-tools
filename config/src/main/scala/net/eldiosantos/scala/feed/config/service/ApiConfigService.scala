package net.eldiosantos.scala.feed.config.service

import com.typesafe.config.Config
import net.eldiosantos.scala.feed.config.model.ApiConfig

class ApiConfigService(config: Config) {

  def load(): ApiConfig = {
    val conf = config.getConfig("api")
    ApiConfig(
      conf.getString("host")
      , conf.getInt("port")
      , conf.getString("remote-actor-path")
    )
  }
}

object ApiConfigService extends ConfigLoader {
  def apply(config: Config): ApiConfigService = new ApiConfigService(config)
  def apply(): ApiConfigService = apply(chooseConfigFile())
}
