package net.eldiosantos.scala.feed.config.service

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.logging.log4j.scala.Logging

trait ConfigLoader extends Logging {

  def chooseConfigFile(): Config = {
    val path = new File(classOf[ConfigLoader].getProtectionDomain.getCodeSource.getLocation.toURI.getPath)
    val configFile = new File(s"${path.getParent}/application.conf")
    if(configFile.exists()) {
      logger.info(s"loading external config file: ${configFile}")
      ConfigFactory.parseFile(configFile)
    } else {
      logger.info(s"loading default config file")
      ConfigFactory.load()
    }
  }
}
