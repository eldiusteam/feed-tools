package net.eldiosantos.scala.feed.config.service

import com.typesafe.config.Config
import net.eldiosantos.scala.feed.config.model.FetcherConfig

class FetcherConfigService(config: Config) {

  def load(): FetcherConfig = {
    val conf = config.getConfig("fetcher")
    FetcherConfig(
      conf.getInt("update-interval-in-minutes")
      , conf.getInt("start-delay-in-minutes")
      , conf.getInt("workers")
    )
  }
}

object FetcherConfigService extends ConfigLoader {
  def apply(config: Config): FetcherConfigService = new FetcherConfigService(config)
  def apply(): FetcherConfigService = apply(chooseConfigFile())
}
