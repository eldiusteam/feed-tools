package net.eldiosantos.scala.feed.config.service

import com.typesafe.config.Config
import net.eldiosantos.scala.feed.config.model.PersistenceConfig

class PersistenceConfigService(config:Config) {

  def load(): PersistenceConfig = {
    val conf = config.getConfig("persistence")
    val driver = if (conf.hasPath("driver")) Option(conf.getString("driver"))
    else Option.empty
    PersistenceConfig(
      conf.getString("url")
      , conf.getString("user")
      , conf.getString("pass")
      , driver
      , conf.getInt("max-total-connections")
      , conf.getInt("max-idle-connections")
      , conf.getInt("validation-timeout")
      , conf.getInt("workers")
    )
  }
}

object PersistenceConfigService extends ConfigLoader {
  def apply(config: Config): PersistenceConfigService = new PersistenceConfigService(config)
  def apply(): PersistenceConfigService = apply(chooseConfigFile())
}
