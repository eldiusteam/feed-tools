package net.eldiosantos.scala.feed.config.service

import org.apache.logging.log4j.scala.Logging

object ConfigService extends Logging {
/*
  private def load(): FeedConfig = {
    def chooseConfigFile(): Config = {
      val path = new File(classOf[FeedConfig].getProtectionDomain.getCodeSource.getLocation.toURI.getPath)
      val configFile = new File(s"${path.getParent}/application.conf")
      if(configFile.exists()) {
        logger.info(s"loading external config file: ${configFile}")
        ConfigFactory.parseFile(configFile)
      } else {
        logger.info(s"loading default config file")
        ConfigFactory.load()
      }
    }

    def loadAuthPersistence(config: Config): PersistenceConfig = {
      val conf = config.getConfig("auth")
      val driver = if (conf.hasPath("driver")) Option(conf.getString("driver"))
      else Option.empty
      PersistenceConfig(conf.getString("url"), conf.getString("user"), conf.getString("pass"), driver)
    }

    def loadPersistence(config: Config): PersistenceConfig = {
      val conf = config.getConfig("persistence")
      val driver = if (conf.hasPath("driver")) Option(conf.getString("driver"))
      else Option.empty
      PersistenceConfig(conf.getString("url"), conf.getString("user"), conf.getString("pass"), driver)
    }

    def loadAgent(config: Config): AgentConfig = {
      val conf = config.getConfig("agent")
      AgentConfig(
        conf.getInt("update-interval-in-minutes")
        , conf.getInt("start-delay-in-minutes")
        , conf.getInt("fetcher-agents")
      )
    }

    def loadApi(config: Config): ApiConfig = {
      val conf = config.getConfig("api")
      ApiConfig(
        conf.getString("host")
        , conf.getInt("port")
      )
    }

    val conf = chooseConfigFile().getConfig("feed-config")

    logger.debug(
      s"""
        |configuration:
        |${conf.toString}
      """.stripMargin)

    FeedConfig(loadPersistence(conf), loadApi(conf), loadAgent(conf), loadAuthPersistence(conf))
  }

  val config = load()

  logger.info(
    s"""----------------------------------------------------------------
      |Readded content:
      |${config}
      |----------------------------------------------------------------
    """.stripMargin)

  def persistence = config.persistence

  def agent = config.agent

  def authPersistence = config.authPersistence

  def api = config.apiConfig
*/
}
