package net.eldiosantos.scala.feed.config.model

case class PersistenceConfig(url: String, user: String, pass: String, driver: Option[String], maxTotalConnections: Int, maxIdleConnections: Int, validationTimeout: Int, workers: Int, debugConnectionPool: Boolean = false)

case class FetcherConfig(updateIntervalInMinutes: Int, startDelayInMinutes: Int, workers: Int)

case class StackElement(name: String, url: String)

case class ApiConfig(host: String, port: Int, remoteActorPath: String)
