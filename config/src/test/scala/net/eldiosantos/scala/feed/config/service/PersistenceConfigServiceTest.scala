package net.eldiosantos.scala.feed.config.service

import org.specs2.mutable.Specification

class PersistenceConfigServiceTest extends Specification {

  "PersistenceConfigServiceTest" should {
    "load" in {
      val config = PersistenceConfigService().load()
      config.driver.isEmpty must beTrue
      config.user mustEqual "user"
      config.pass mustEqual "pass"
      config.url  mustEqual "jdbc:h2:mem:target/database.db"
    }
  }
}
