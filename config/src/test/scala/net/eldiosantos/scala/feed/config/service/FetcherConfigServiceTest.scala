package net.eldiosantos.scala.feed.config.service

import org.specs2.mutable.Specification

class FetcherConfigServiceTest extends Specification {

  "FetcherConfigServiceTest" should {
    "load" in {
      val config = FetcherConfigService().load()
      config.updateIntervalInMinutes mustEqual 10
      config.workers mustEqual 5
      config.startDelayInMinutes mustEqual 1
    }
  }
}
