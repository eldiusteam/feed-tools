package net.eldiosantos.scala.feed.model.domain

import org.specs2.mutable.Specification

class FeedTypeEnumTest extends Specification {

  "FeedTypeEnumTest" should {
    "byName" in {
      FeedTypeEnum.values.foreach(v => println(s"\t- value: ${v}"))
      FeedTypeEnum.byName("RSS").get mustEqual FeedTypeEnum.RSS
      FeedTypeEnum.byName("INVALID") must beNone
    }

    "valueToFeedType" in {
      ok
    }

  }
}
