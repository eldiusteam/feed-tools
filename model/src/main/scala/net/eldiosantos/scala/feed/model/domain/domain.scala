package net.eldiosantos.scala.feed.model.domain

import net.eldiosantos.scala.feed.model.domain.FeedTypeEnum.FeedType

case class FeedSpec(url: String, name: String = "")

case class Feed(url: String, title: String, description: String, language: String, copyright: String, lastBuildDate: String, imageUrl: String, imageTitle: String, feed: List[FeedItem], feedType: FeedType = FeedTypeEnum.RSS) {
  def isPodcast: Boolean = feed.exists(_.isPodcast)
}

case class FeedItem(id: String, url: String, title: String, content: String, episodeUrl: String, episodeLength: String, episodeType: String, pubDate: String) {
  def isPodcast: Boolean = !episodeUrl.isEmpty
}

object FeedTypeEnum extends Enumeration {
  case class FeedType(name: String) extends super.Val

  implicit def valueToFeedType(x: Value): FeedType = x.asInstanceOf[FeedType]
  type Type = Value
  val RSS = FeedType("RSS")

  def byName(name: String): Option[FeedType] = {
    FeedTypeEnum.values.toList.map(valueToFeedType).find(_.name.equalsIgnoreCase(name))
  }
}
