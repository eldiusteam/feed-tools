package net.eldiosantos.scala.feed.api.actor

import akka.actor.Actor
import net.eldiosantos.scala.feed.api.actor.RemoteCallerActor.AddFeed
import net.eldiosantos.scala.feed.config.service.ApiConfigService
import net.eldiosantos.scala.feed.model.domain.FeedSpec
import org.apache.logging.log4j.scala.Logging

class RemoteCallerActor extends Actor with Logging {
  private val config = ApiConfigService().load()

  private val selection = context.actorSelection(config.remoteActorPath)

  logger.info(s"remote actor: ${config.remoteActorPath}")

  override def receive: Receive = {
    case AddFeed(feed) => try {
      selection ! feed
      //ref ! FetchSingle(feed)
      //actor ! FetchSingle(feed)
    } catch {
      case t: Throwable => logger.error("Error calling remote actor", t)
    }
  }
}

object RemoteCallerActor {
  case class AddFeed(feed: FeedSpec)
}
