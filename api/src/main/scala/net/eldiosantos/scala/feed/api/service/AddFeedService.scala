package net.eldiosantos.scala.feed.api.service

import akka.actor.{ActorSystem, Props}
import net.eldiosantos.scala.feed.api.actor.RemoteCallerActor
import net.eldiosantos.scala.feed.api.actor.RemoteCallerActor.AddFeed
import net.eldiosantos.scala.feed.model.domain.FeedSpec

class AddFeedService {
  private val system = ActorSystem("feed-fetcher-system")
  private val caller = system.actorOf(Props[RemoteCallerActor])

  def add(feed: FeedSpec): Unit = {
    caller ! AddFeed(feed)
  }
}

object AddFeedService {
  private val service = new AddFeedService

  def add(feed: FeedSpec): Unit = service.add(feed)
}
