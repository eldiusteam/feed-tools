
resolvers ++= Seq (
  "Typesafe Repository" at "https://repo.typesafe.com/typesafe/releases/"
  //, "SBT Android" at "https://dl.bintray.com/pfn/sbt-plugins"
  , Resolver.jcenterRepo
)

libraryDependencies ++=Seq(
  //"com.spotify" % "docker-client" % "3.5.13"
  "org.vafer" % "jdeb" % "1.3" artifacts (Artifact("jdeb", "jar", "jar"))
)

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1")
addSbtPlugin("com.codacy" % "sbt-codacy-coverage" % "1.3.12")
addSbtPlugin("com.eed3si9n" % "sbt-unidoc" % "0.4.1")
addSbtPlugin("org.foundweekends" % "sbt-bintray" % "0.5.4")
addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.8")

addSbtPlugin("com.eed3si9n" %% "sbt-assembly" % "0.14.6")
//addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.9.0")
