# feed-tools project #

I started this project to learn how to use Akka
framework and to test some approaches with Scala
language.

## Status ##

[![CircleCI](https://circleci.com/bb/eldiusteam/feed-tools.svg?style=svg)](https://circleci.com/bb/eldiusteam/feed-tools)
[![codecov](https://codecov.io/bb/eldiusteam/feed-tools/branch/master/graph/badge.svg)](https://codecov.io/bb/eldiusteam/feed-tools)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/7bccec3984ce4f0eac97b888723f072e)](https://www.codacy.com/app/eldiosantos/feed-tools?utm_source=eldiusteam@bitbucket.org&amp;utm_medium=referral&amp;utm_content=eldiusteam/feed-tools&amp;utm_campaign=Badge_Grade)
[ ![Download](https://api.bintray.com/packages/eldius/feed-tools/feed-tools/images/download.svg) ](https://bintray.com/eldius/feed-tools/feed-tools/_latestVersion)

## Package projects ##

- Docker packager: [Eldius/feed-tools-docker](https://bitbucket.org/Eldius/feed-tools-docker/)
- Deb packager: [eldiusteam/feed-tools-deb](https://bitbucket.org/eldiusteam/feed-tools-deb/)
