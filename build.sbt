
resolvers ++= Seq(
  Resolver.jcenterRepo
  , DefaultMavenRepository
  , "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
)

val akkaVersion = "2.5.12"
val akkaHttpVersion = "10.1.1"
val specs2Version = "4.1.0"
val elastic4sVersion = "6.2.6"

lazy val commonSettings = Seq(
  organization := "net.eldiosantos.scala.feed",
  scalaVersion := "2.12.6",
  scalacOptions in (Compile,doc) := Seq("-groups", "-implicits"),
  autoAPIMappings := true,
  bintrayRepository := "feed-tools",
  licenses += ("MIT", url("http://opensource.org/licenses/MIT")),
  // test in assembly := {},
  parallelExecution := false,
  libraryDependencies ++= Seq(
    "org.specs2" %% "specs2-core" % specs2Version % Test
    , "org.specs2" %% "specs2-matcher-extra" % specs2Version % Test
    , "org.specs2" %% "specs2-matcher" % specs2Version % Test
  ),
  scalacOptions in Test ++= Seq("-Yrangepos")
)

lazy val config = (project in file("config")).
  settings(commonSettings: _*).
  settings(
    name := "feed-config",
    libraryDependencies ++= Seq(
      "com.typesafe" % "config" % "1.3.1"
      , "org.apache.logging.log4j" % "log4j-api" % "2.10.0"
      , "org.apache.logging.log4j" % "log4j-core" % "2.10.0"
      , "org.apache.logging.log4j" %% "log4j-api-scala" % "11.0"
    )
  )



lazy val model = (project in file("model")).
  settings(commonSettings: _*).
  settings(
    name := "feed-model"
  )

lazy val parser = (project in file("parser")).
  settings(commonSettings: _*).
  dependsOn(model).
  settings(
    name := "feed-parser",
    libraryDependencies ++= Seq(
      "org.scala-lang.modules" %% "scala-xml" % "1.0.6"
      , "org.apache.logging.log4j" % "log4j-api" % "2.10.0"
      , "org.apache.logging.log4j" % "log4j-core" % "2.10.0"
      , "org.apache.logging.log4j" %% "log4j-api-scala" % "11.0"
    )
  )

lazy val persistence = (project in file("persistence")).
  settings(commonSettings: _*).
  dependsOn(config, model).
  settings(
    name := "feed-persistence",
    libraryDependencies ++= Seq(
      "com.h2database" % "h2" % "1.4.196" % Test
      , "org.apache.commons" % "commons-dbcp2" % "2.2.0"
      , "org.apache.logging.log4j" % "log4j-api" % "2.10.0"
      , "org.apache.logging.log4j" % "log4j-core" % "2.10.0"
      , "org.apache.logging.log4j" %% "log4j-api-scala" % "11.0"
      , "com.typesafe.akka" %% "akka-actor" % akkaVersion
      , "com.typesafe.akka" %% "akka-slf4j" % akkaVersion
    )
  )

lazy val fetcher = (project in file("fetcher")).
  settings(commonSettings: _*).
  dependsOn(model, config, parser, persistence).
  settings(
    name := "feed-fetcher",
    mainClass in Compile := Some("net.eldiosantos.scala.feed.fetcher.FetcherAppObject"),
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor" % akkaVersion
      , "com.typesafe.akka" %% "akka-slf4j" % akkaVersion
      //, "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test
      //, "org.scalatest" %% "scalatest" % "3.0.5" % Test
      , "org.apache.logging.log4j" % "log4j-slf4j-impl" % "2.9.1"
      , "com.h2database" % "h2" % "1.4.196"
      , "org.apache.logging.log4j" % "log4j-api" % "2.10.0"
      , "org.apache.logging.log4j" % "log4j-core" % "2.10.0"
      , "org.apache.logging.log4j" %% "log4j-api-scala" % "11.0"
      , "mysql" % "mysql-connector-java" % "8.0.11"
    )
  )

lazy val api = (project in file("api")).
  settings(commonSettings: _*).
  dependsOn(config, model, persistence).
  settings(
    name := "feed-api",
    mainClass in Compile := Some("net.eldiosantos.scala.feed.api.ApiApp"),
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-remote" % akkaVersion
      , "mysql" % "mysql-connector-java" % "8.0.11"
      , "com.typesafe.akka" %% "akka-actor" % akkaVersion
      , "com.typesafe.akka" %% "akka-stream" % akkaVersion
      , "com.typesafe.akka" %% "akka-testkit" % akkaVersion
      , "com.typesafe.akka" %% "akka-http" % akkaHttpVersion
      , "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion
    )
  )

val root = (project in file(".")).
  settings(commonSettings: _*).
  enablePlugins(ScalaUnidocPlugin).
  settings(
    name := "feed-tools"
  ).
  aggregate(
    model
    , config
    , parser
    , persistence
    , fetcher
  )
